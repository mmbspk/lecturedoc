package de.lecturedoc.tools

import java.io._
import java.util.jar._
import scala.Console
import java.util.jar.JarInputStream

class LectureDocInit {
  
    
  def initExtractor(args: Array[String]): Unit = {  

      val path = new java.io.File(".").getCanonicalPath
      var buffer = new Array[Byte](1024)

      //Filtering the Jar files for further processing  
      def accept(file: File): Boolean =
        List(".jar").find { ext => file.getName.endsWith(ext) } != None

      val fn = new File(path)
      if (fn.isDirectory) {
        val files = fn.listFiles.filter { f => accept(f) }
        files.foreach { f => extractJar(f) }
      } else if (accept(fn))
        extractJar(fn)

      def extractJar(file: File): Unit = {

        val basename = file.getName.substring(0, file.getName.lastIndexOf("."))
        val sourceDir = new File(file.getParentFile, "")
        val destDir = new File(args(1))
        val jar = new JarFile(file)
        val enu = jar.entries

        println("===> Processing File: " + file)

        //Check for Folder Existence
        if (new File(destDir + "/Library").isDirectory()) {
          print("Folder Already Exists, Do you Want to Replace(R) Update(U) Abort(A) (R/U/A): ")
          val conInp = System.console.readLine()
          if (conInp == "R" || conInp == "r") {
            print("Doing this will delete and replace all the contents of Folder, do you want to continue Yes/No(Abort) (Y/N): ")
            
            val conInp1 = System.console.readLine()
            if (conInp1 == "Y" || conInp1 == "y"){ 
              loopJarEntries(false)
              } else {
              System.exit(0)
              }
          } else if (conInp == "U" || conInp == "u") loopJarEntries(true)
          else System.exit(0)
        } else loopJarEntries(false)

        //Loop through the main Jar contents
        def loopJarEntries(update: Boolean) = {
          while (enu.hasMoreElements) {
            val entry = enu.nextElement
            val name = entry.getName

            //Search for inner jar
            if (entry.getName.endsWith(".jar") && entry.getName().contains("main")) {
              val secJarName = entry.getName().substring(entry.getName().indexOf("/") + 1, entry.getName().length())
              val jarIn = new JarInputStream(jar.getInputStream(entry))
              loopInnerJar(jarIn, secJarName, update)
              if (!update) println("===> Folder has been created/Replaced at destination: " + destDir)
              else println("===> Above file(s) have been updated in destination folder")
              System.exit(0)
            }
          }
        }

        //Loop through the contents of inner jar
        def loopInnerJar(jarIn: JarInputStream, secJarName: String, update: Boolean) = {
          var jarEntry = jarIn.getNextJarEntry()
          while (jarEntry != null) {
            val name = jarEntry.getName()
            val entryPath =
              if (name.startsWith(secJarName)) name.substring(secJarName.length)
              else name

            if (entryPath.contains("Library")) {
              if (jarEntry.isDirectory) {
                if (update) {
                  if (!new File(destDir, entryPath).isDirectory())
                    new File(destDir, entryPath).mkdirs
                } else new File(destDir, entryPath).mkdirs
              } else {
                if (update && new File(destDir, entryPath).exists()) {
                  var oldFile = new File(destDir, entryPath).lastModified()
                  var newFile = jarEntry.getTime()
                  if (newFile > oldFile) {                    
                    println("File Updated: " + jarEntry.getName())
                    copyStream(entryPath, jarIn, buffer)
                  }
                } else {
                  copyStream(entryPath, jarIn, buffer)
                }
              }
            }
            jarEntry = jarIn.getNextJarEntry()
          }
        }

        //Copying the individual streams
        def copyStream(entryPath: String, jarIn: JarInputStream, buffer: Array[Byte]) = {

          val ostream = new FileOutputStream(new File(destDir, entryPath))
          var len = -1
          while ({ len = jarIn.read(buffer, 0, 1024); len != -1 })
            ostream.write(buffer, 0, len)

          ostream.close
        }

      }
    }  

}