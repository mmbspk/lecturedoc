package eu.henkelmann.actuarius

import java.io.{ InputStreamReader, StringWriter }
import scala.io.Source
import java.util.zip.ZipFile
import java.util.Enumeration
import java.util.zip.ZipEntry
import java.util.Date
import java.io.File
import java.io.OutputStream
import java.io.InputStream
import java.io.IOException
import java.io.FileOutputStream
import scala.io.Codec
import java.text.DateFormat

// TODO Add support for definition lists
// TODO Add support for table definitions
// TODO Add support for markdown parsing in XML tags, e.g., <table markdown="true">...</table>
// TODO Copy the library stuff from the JAR to the document folder where the source document resides if it is not already found in that folder.

/**
  * Contains a main method that simply reads everything from stdin, parses it as LectureDoc and
  * prints the result to stdout.
  */
object LectureDoc extends Transformer {

    def process(in: java.io.InputStream, out: java.io.OutputStream, complete: Boolean, lastMofified: Date) {

        val printStream = new java.io.PrintStream(out, true, "UTF-8")

        def println(s: String) { printStream.println(s) }

        def print(s: String) { printStream.print(s) }

        //read from system input stream
        val input = Source.fromInputStream(in)(Codec.UTF8).mkString

        //run that string through the transformer trait's apply method
        try {
            val output = apply(input)

            if (complete) {
                // TODO [Refine] replace the hard coded HTML using a template language (e.g., velocity)
                // TODO [Refine] make it possible to add additional header fragments          
                println("<!DOCTYPE html><html><head><meta charset=\"utf-8\"><meta name=generator content=\"LectureDoc\">")
                println(Source.fromInputStream(this.getClass().getClassLoader().getResourceAsStream("Library/header.fragment.html")).mkString)
                println("</head><body data-ldjs-last-modified=\""+lastMofified.getTime()+"\">")
				println("<div id=\"body-content\">")
                print(output)
				println("</div>")
                println("</body></html>")
            }
            else {
                print(output)
            }
        }
        catch {
            case e: Exception ⇒ {
                println("Conversion failed: "+e.getLocalizedMessage())
                System.out.println("Conversion failed (see generated file for details!)")
            }
        }
    }

    private def printUsageAndExit() {
        // TODO [Refine] move the help text to a better location
        println("Usage (Standard Mode): java eu.henkelmann.actuarius [--complete] < \"Input\" > \"Output\"")
        println("Usage (Server Mode): java eu.henkelmann.actuarius [--complete] --server [\"existing directory with write access\"]")
        println("In the server mode, the given directory is watched for changes and additions of "+
            "LectureDoc files. When a change/addition is detected the file is converted to HTML.")

        System.exit(-1)
    }

    def main(args: Array[String]): Unit = {
        if (args.length > 3) printUsageAndExit();

        val complete = args.contains("--complete")
        val server = args.contains("--server")
        val init = args.contains("--init")
        
        //Initial Copying of the Library contents require for HTML
        if(init){
          import de.lecturedoc.tools.LectureDocInit
          
          val ldc = new LectureDocInit()
          ldc.initExtractor(args)
        }

        if (server) {
            // Server mode. 
            // I.e., we watch a directory for changes and create the new LectureDoc files when needed
            import java.nio.file._
            import java.nio.file.StandardWatchEventKinds._
            import scala.collection.JavaConversions._

            val path: Path =
                if (complete && args.length == 3) { Paths.get(args(2)); }
                else if (!complete && args.length == 2) { Paths.get(args(1)); }
                else { Paths.get(System.getProperty("user.dir")) }

            if (!(Files.isWritable(path) && Files.isDirectory(path))) printUsageAndExit()

            println("Watching directory: "+path.toAbsolutePath()+" for changes.")

            val watchService = FileSystems.getDefault().newWatchService();
            path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
            var watchKey: WatchKey = null
            do {
                watchKey = try { watchService.take() } catch { case e: InterruptedException ⇒ return }
                for {
                    event ← watchKey.pollEvents()
                    if event.kind() ne OVERFLOW
                    file = path.resolve(event.context().asInstanceOf[Path])
                    fileName = file.toString
                    if fileName.endsWith(".md")
                } {
                    // Convert the updated/new lecture doc file
                    val date = new java.util.Date(Files.getLastModifiedTime(file).toMillis())
                    println("Converting file: "+fileName+" last modified: "+DateFormat.getDateTimeInstance().format(date))
                    val newFileName = fileName.substring(0, fileName.length() - 3)+".html"
                    val newFile = Paths.get(newFileName)
                    Files.deleteIfExists(newFile);

                    var in: InputStream = null
                    var out: OutputStream = null

                    try {
                        in = Files.newInputStream(file)
                        out = Files.newOutputStream(newFile)
                        process(in, out, complete, date)
                    }
                    catch {
                        case e: IOException ⇒
                            System.err.println("Error while converting: "+file+" ("+e.getMessage()+")")
                    }
                    finally {
                        if (in != null)
                            in.close()
                        if (out != null)
                            out.close();
                    }
                }
            } while (watchKey.reset())

            System.err.println("Watching the path: "+path+" was aborted.")
        }
        else {
            // Standard mode
            process(System.in, System.out, complete, new java.util.Date() /*last-modified*/ )
        }
    }

    ///*
    // * Copyright (C) 2010 The Android Open Source Project
    // *
    // * Licensed under the Apache License, Version 2.0 (the "License");
    // * you may not use this file except in compliance with the License.
    // * You may obtain a copy of the License at
    // *
    // * http://www.apache.org/licenses/LICENSE-2.0
    // *
    // * Unless required by applicable law or agreed to in writing, software
    // * distributed under the License is distributed on an "AS IS" BASIS,
    // * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    // * See the License for the specific language governing permissions and
    // * limitations under the License.
    // */
    //
    ////package com.google.doclava;
    //
    //import java.io.File;
    //import java.io.FileOutputStream;
    //import java.io.IOException;
    //import java.io.InputStream;
    //import java.net.URL;
    //import java.util.Enumeration;
    //import java.util.jar.JarEntry;
    //import java.util.jar.JarFile;
    //
    //public class JarUtils {
    //  /**
    //   * Returns the jar file used to load class clazz, or defaultJar if clazz was not loaded from a
    //   * jar.
    //   */
    //  public static JarFile jarForClass(Class<?> clazz, JarFile defaultJar) {
    //    String path = "/" + clazz.getName().replace('.', '/') + ".class";
    //    URL jarUrl = clazz.getResource(path);
    //    if (jarUrl == null) {
    //      return defaultJar;
    //    }
    //
    //    String url = jarUrl.toString();
    //    int bang = url.indexOf("!");
    //    String JAR_URI_PREFIX = "jar:file:";
    //    if (url.startsWith(JAR_URI_PREFIX) && bang != -1) {
    //      try {
    //        return new JarFile(url.substring(JAR_URI_PREFIX.length(), bang));
    //      } catch (IOException e) {
    //        throw new IllegalStateException("Error loading jar file.", e);
    //      }
    //    } else {
    //      return defaultJar;
    //    }
    //  }
    //
    //  /**
    //   * Copies a directory from a jar file to an external directory.
    //   */
    //  public static void copyResourcesToDirectory(JarFile fromJar, String jarDir, String destDir)
    //      throws IOException {
    //    for (Enumeration<JarEntry> entries = fromJar.entries(); entries.hasMoreElements();) {
    //      JarEntry entry = entries.nextElement();
    //      if (entry.getName().startsWith(jarDir + "/") && !entry.isDirectory()) {
    //        File dest = new File(destDir + "/" + entry.getName().substring(jarDir.length() + 1));
    //        File parent = dest.getParentFile();
    //        if (parent != null) {
    //          parent.mkdirs();
    //        }
    //
    //        FileOutputStream out = new FileOutputStream(dest);
    //        InputStream in = fromJar.getInputStream(entry);
    //
    //        try {
    //          byte[] buffer = new byte[8 * 1024];
    //
    //          int s = 0;
    //          while ((s = in.read(buffer)) > 0) {
    //            out.write(buffer, 0, s);
    //          }
    //        } catch (IOException e) {
    //          throw new IOException("Could not copy asset from jar file", e);
    //        } finally {
    //          try {
    //            in.close();
    //          } catch (IOException ignored) {}
    //          try {
    //            out.close();
    //          } catch (IOException ignored) {}
    //        }
    //      }
    //    }
    //
    //  }
    //
    //  private JarUtils() {} // non-instantiable
    //}

    //            /* 
    //	 * Copies an entire folder out of a jar to a physical location.  
    //	 */
    //    def copyJarFolder(jarName : String , folderName : String ) {
    //			var z = new ZipFile(jarName);
    //			var entries = z.entries();
    //			while (entries.hasMoreElements()) {
    //				var entry = entries.nextElement().asInstanceOf[ZipEntry];
    //				if (entry.getName().contains(folderName)) {
    //					var f = new File(entry.getName());
    //					if (entry.isDirectory()) {
    //						f.mkdir();
    //					}
    //					else if (!f.exists()) {
    //						if (copyFromJar(entry.getName(), new File(entry.getName()))) {
    //							System.out.println("Copied: " + entry.getName());
    //						}
    //					}
    //				}
    //			}
    //
    //	}

    //
    //        /* 
    //	 * Copies a file out of the jar to a physical location.  
    //	 *    Doesn't need to be private, uses a resource stream, so may have
    //	 *    security errors if ran from webstart application 
    //	 */
    //	def copyFromJar(sResource : String, fDest : File) : Boolean = {
    //		if (sResource == null || fDest == null) return false;
    //		var sIn : InputStream = null;
    //		var sOut : OutputStream = null;
    //		var sFile : File= null;
    //			fDest.getParentFile().mkdirs();
    //			sFile = new File(sResource);
    //
    //		try {
    //			var nLen = 0;
    //			sIn = this.getClass().getResourceAsStream(sResource);
    //			if (sIn == null)
    //				throw new IOException("Error copying from jar "  + 
    //					"(" + sResource + " to " + fDest.getPath() + ")");
    //			sOut = new FileOutputStream(fDest);
    //			var bBuffer : Array[Byte] = new Array[Byte](1024);
    //			while ((nLen = sIn.read(bBuffer)) > 0)
    //				sOut.write(bBuffer, 0, nLen);
    //			sOut.flush();
    //		}
    //			finally {
    //			try {
    //				if (sIn != null)
    //					sIn.close();
    //				if (sOut != null)
    //					sOut.close();
    //			}
    //		}
    //		return fDest.exists();
    //	}
}
