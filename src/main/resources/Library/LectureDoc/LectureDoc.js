/*
   Copyright 2012 Michael Eichberg et al - www.michael-eichberg.de

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

// TODO Add some high-level document regarding the fundamental architecture/flow
// TODO Add a posibility to "write/type text" on a slide
// TODO Create the overview based on the set of current slides (to make annotations visible!) 
// TODO Add support for persistent per slide notes.
// TODO Add multitouch support.
// TODO Add support for the management of server-side notes
 
/*
	The following script turns a browser in a fully-fledged presentation program (if 
	the browser has a presentation mode/a chromeless mode).
	The basic idea is that we render a standard HTML document that uses some
	predefined CSS classes as a set of slides. 
	
	The idea is that all SECTION elements where the CLASS attribute contains "slide"
	are rendered as single slides and the user can change the slides using the
	standard controls (left, right keys etc.) known from other presentation programs.

	@author Michael Eichberg
*/
var LectureDoc = (function() {
	
	/** 
		Converts, e.g., a live NodeList into an array. 
	*/
	function toArray(nodeList) { return Array.prototype.slice.call(nodeList); };

	// 
	// The following variables contain immutable state.
	// 

	/**
	 	A copy of the original document body.
	 	
	 	@final
	*/ 
	var theDocumentBody;
			
	/**
		The list of all SECTION elements where the CLASS attribute contains "slide".
		
		A section that represents a slide must not contain other SECTION elements
		where the CLASS attribute also contains "slide". Hence, using a SECTION element
		with any other class is still possible. 
		
		If a slide contains another slide, the result (rendering of the slide) is not 
		defined and may change without further notice.
		
		@final
	*/
	var theSlides;
	
	/**
		The list of all titles (the value of the TITLE attribute) of the SECTION
		elements that make up this lecture's slides.
		
		@final
	*/
	var theListOfSlides;

	//
	// The following variables contain mutable state that is related to the presentation
	// as a whole.
	//
	
	/**
		The id (number) of the current slide that is shown.
	*/
	var currentSlide = undefined;

	function bodyContent() {
		return document.getElementById('body-content');
	}

	function analyzeDocument() {
		theDocumentBody = document.body.cloneNode(true);
		theSlides = toArray(document.querySelectorAll("section.slide")); 
		theListOfSlides = createListOfSlides(theSlides);
	}

	/**
		Initialization that is independent of the current mode (presentation, continuous, light table,...),
		but which makes sense, if and only if, we have at least one slide.
	*/
	function enableLectureDoc() {
		document.addEventListener(
			"keyup", 
			function(event) {
				console.log(event);
				var target = event.target.tagName;
				if( target === "TEXTAREA" || 
					target === "INPUT" ||
					event.altKey || 
					event.ctrlKey || 
					event.metaKey || 
					event.shiftKey ) {
					return;
				}
				
				if (event.keyCode == 77 /*m*/) { 
					// Select how the slides are presented. 
					selectMode();
				}			
			}, 
			true
		);
	}

	/**
		Renders a screen to enable the user to select how the current document is rendered.
		
		TODO Improve the design of the mode selection.
	*/
	function selectMode() {
	
		var theModes = document.createElement("div");
		theModes.id = "ldjs-modes";
		theModes.className = "ldjs-message ldjs-message-information";	
	
		var allModes = document.createElement("ol");
		theModes.appendChild(allModes);

		var pm = document.createElement("li");
		pm.appendChild(document.createTextNode("Presentation Mode"));
		pm.onclick = function(){ displaySlide(); }
		allModes.appendChild(pm);

		var sdl = document.createElement("li");
		sdl.appendChild(document.createTextNode("Standard Document Layout"));
		sdl.onclick = function(){ renderAsDocument(); }
		allModes.appendChild(sdl);
		
		var cl = document.createElement("li");
		cl.appendChild(document.createTextNode("Compact Layout"));
		cl.onclick = function(){ renderAsLectureNotes(); }
		allModes.appendChild(cl);
		
		
		var lt = document.createElement("li");
		lt.appendChild(document.createTextNode("Light Table"));
		lt.onclick = function(){ renderLightTable(); }
		allModes.appendChild(lt);
		
		var sc = document.createElement("li");
		sc.appendChild(document.createTextNode("All Slides After Each Other (e.g., for creating a PDF)"));
		sc.onclick = function(){ renderContinuousRepresentation(); }
		allModes.appendChild(sc);
		
		clear();
		bodyContent().appendChild(theModes);		
	}

	
	/**
	 * @param {Integer} slide - The id of the slide (zero-based) that should be shown. If the
	 *					id is larger than the number of slides, the id is ignored and the first
	 *					slide will be shown.
	 */
	function setupPresentationMode(slide) {
		addSlideNumbers(theSlides);
							
		// Register event handlers to make it possible to steer and manipulate the 
		// presentation
		//
		console.log("Activating presentation controls.");
				
		activateLaserPointerSupport();
			
		theSlides.forEach(initializeNavMenu);
		theSlides.forEach(addCanvasLayer);			

		// Show the "first slide". 
		// 
		// Implementation note: Between the last time the presentation was shown and this
		// time the number of slides may have changed. If the number was reduced, we
		// just start with the first slide and show a corresponding message.
		currentSlide = slide || 0;
		if(currentSlide > theSlides.length){
			currentSlide = 0;
			displaySlide();
			addMessageBox(
				"ldjs-presentation-reset-warning",
				"The number of slides has changed.<br>Restarting the presentation with the first slide.",
				undefined,
				5000,
				"ldjs-message ldjs-message-error");
		} else {
			displaySlide();
		}
	}
	
	/**
		Creates a list of slides based on the titles of a given set of slides (SECTION 
		elements with class "slide" and where the title attribute is set).
		
		@return A DIV element with the id "ldjs-listofslides" which contains the list of slides. 
	*/
	function createListOfSlides(theSlides){
		var theListOfSlides = document.createElement("div");
		theListOfSlides.id = "ldjs-listofslides";
		theListOfSlides.className = "ldjs-message ldjs-message-information";	
	
		var listEntries = document.createElement("ol");
		for(var s = 0 ; s < theSlides.length ; s++){
			var title = theSlides[s].dataset.title;	
			if(title){
				var listEntry = document.createElement("li");
				listEntry.appendChild(document.createTextNode(title));
				listEntry.onclick = jumpToSlide(s);
				listEntries.appendChild(listEntry);
			}
		}
		theListOfSlides.appendChild(listEntries);
		
		return theListOfSlides;
	}
	
	/**
		Adds slide numbers to all extracted slides.
		
		FIXME The size of the slide number is not the same on all slides.
	*/
	function addSlideNumbers(theSlides){
		for(var s = 0 ; s < theSlides.length ; s++){
			var slideNumber = document.createElement("div");
			slideNumber.innerHTML = s + 1;
			slideNumber.className = "ldjs-slide-number";
			theSlides[s].firstChild.appendChild(slideNumber); // TODO make the .firstChild selector more stable!
		}
	}
	

	/**
		Enables the virtual laser pointer. The virtual laser pointer is basically, 
		a DIV element that is added as the first element to the BODY element and
		which uses fixed positioning.
	*/	
	function activateLaserPointerSupport() {
		var theLaserPointer = document.createElement("div");
		theLaserPointer.id = "ldjs-laserpointer";
		
		document.body.addEventListener(
			'mousemove',
			function(event) {	
				if(event.ctrlKey){
					// When the slide was changed the laser pointer may belong to
					// the wrong section element; i.e. a section element belonging
					// to a slide that is not shown; repair if necessary.
					if(theLaserPointer.parentNode !== document.body){
						document.body.insertBefore(theLaserPointer,document.body.firstChild);
					}
					theLaserPointer.style.left = event.clientX+"px"; 
					theLaserPointer.style.top = event.clientY+"px";
					theLaserPointer.style.visibility = "visible";
					event.preventDefault();				
				}
				else {
					theLaserPointer.style.visibility = "hidden";				
				}
			},
			false);
	}
	
	/**
		Returns a function that – when called later on – jumps to the specified slide.
		
		@param {Integer} targetSlide - The valid id ([0...#NumberOfSlides]) of a slide.
		@return A function that - when called later on - causes the presentation to jump 
				to the specified slide.
	*/
	function jumpToSlide(targetSlide){
		return function() { 
			currentSlide = targetSlide; 
			displaySlide(); 
		};
	}
	
	/**
	 	Analyzes a given slide's structure  and - if necessary - creates
		a menu to show the different "ASIDEs". 
	
		@param slide - A SECTION element that represents a slide.
						{<section class="slide">…</section>}
	 */
	function initializeNavMenu(slide){
			
		// Get all ASIDE elements that are direct children of the given slide's 
		// SECTION>Div.section-body element and which have a title.
		var sectionBody = slide.firstChild // TODO make the selector more stable; i.e., replace firstChild by section>div.section-body or something similar
		var asides = toArray(sectionBody.getElementsByTagName("aside")). 
				filter(function(element){return element.parentNode === sectionBody && element.dataset.title;});
		
		if (asides.length > 0){	
			// Look for a NAV element that is a direct child of the SECTION element
			var nav = slide.querySelector("section>div[class='section-body']>nav"); // TODO refactor: use sectionBody as the anchor
			if(!nav){
				nav = document.createElement("NAV");
				sectionBody.appendChild(nav);
			}
			asides.forEach(function(aside){
				var asideLink = document.createElement("div");
				asideLink.className = "ldjs-aside-link";
				asideLink.appendChild(document.createTextNode(aside.dataset.title)); // innerText is not supported by Firefox
				asideLink.onclick = function(){
					if(window.getComputedStyle(aside).getPropertyValue("visibility") === 'hidden'){
						aside.style.visibility = 'visible';
						asideLink.firstChild.appendData(" ✦");
					}else {
						aside.style.visibility = 'hidden';
						var linkText = asideLink.firstChild;
						linkText.deleteData(linkText.data.length-2,2);
					};
				};
				nav.appendChild(asideLink);		
			});
		}	
	}
	
	
	/**
		Adds a canvas layer to a slide. 
		
		Note: the canvas layer is not added to the BODY element, because we want to preserve a 
		the contents of the canvas layer when the slide is changed.
	*/
	function addCanvasLayer(slide){		
		var canvas = document.createElement("div");
		canvas.id = "ldjs-canvas";
		slide.firstChild.insertBefore(canvas,slide.firstChild.firstChild);	// TODO make the selector more stable; i.e., replace firstChild by section>div.section-body or something similar
		
		var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		canvas.appendChild(svg);
		
		var strokeColor = "black";		
		var x = -1; // stores the x position of the mouse (when we receive a mousemove event)
		var y = -1; // stores the y position of the mouse (when we receive a mousemove event)
		// The mouse move event listener is added to the slide and not the canvas
		// because the canvas(svg) is configured to not intercept mouse events to
		// enable users to,e.g., still click on links even if the canvas is shown.
		// CSS Property: 	svg{ pointer-events: none }.
		slide.addEventListener( 
			"mousemove", 
			function(event){
				if(event.altKey && window.getComputedStyle(canvas).getPropertyValue("visibility") === 'visible'){
				//if(event.buttons === 1){
					var new_x = event.clientX - canvas.parentNode.offsetLeft;
					var new_y = event.clientY - canvas.parentNode.offsetTop;
					if (x >= 0 && (new_x !== x || new_y !== y)){ 
						var line = document.createElementNS("http://www.w3.org/2000/svg", "line");
						line.setAttribute("stroke", strokeColor);
						line.setAttribute("stroke-width", "3");					
						line.setAttribute("x1", x);
						line.setAttribute("y1", y);
						line.setAttribute("x2", new_x);
						line.setAttribute("y2", new_y);
						svg.appendChild(line);
					}
					x = new_x;
					y = new_y;				
				}
				else {
					x = -1;
					y = -1;
				}
				return true;
			}
		);
		
		var menu = document.createElement("div");
		menu.id = "ldjs-canvas-menu";
		canvas.appendChild(menu);

		var clearCanvas = document.createElement("div");
		clearCanvas.id = "ldjs-canvas-menu-clear-canvas"; 
		clearCanvas.appendChild(document.createTextNode("✗"));
		clearCanvas.onclick = function(){ 
			while(svg.hasChildNodes()){
				svg.removeChild(svg.firstChild); 
			}
		};
		menu.appendChild(clearCanvas);

		var blackStroke = document.createElement("div");
		blackStroke.id = "ldjs-canvas-menu-black-stroke";
		blackStroke.appendChild(document.createTextNode("◉"));
		blackStroke.onclick = function(){ 
			strokeColor = "black"; 
		};
		menu.appendChild(blackStroke);

		var yellowStroke = document.createElement("div");
		yellowStroke.id = "ldjs-canvas-menu-yellow-stroke";
		yellowStroke.appendChild(document.createTextNode("◉"));
		yellowStroke.onclick = function(){ 
			strokeColor = "yellow";
		};
		menu.appendChild(yellowStroke);

		var redStroke = document.createElement("div");
		redStroke.id = "ldjs-canvas-menu-red-stroke";
		redStroke.appendChild(document.createTextNode("◉"));
		redStroke.onclick = function(){ 
			strokeColor = "red"; 
		};
		menu.appendChild(redStroke);	
	}
	
	
	//
	// The following variables contain state that is only relevant while a particular
	// slide is shown. When the slide changes, the following variables are reset to
	// their default values that are given next.
	//
	
	/**
	 	Array of timeouts (integer values) that should be cancelled, when the slide is changed
	 */
	var timeouts = []; 
	var enteredSlideNumber = undefined; 
	var isBlackedOut = false;
	var isOverviewShown = false;
	var isListOfSlidesShown = false;
	var isHelpShown = false;

	/**
		Resets all variables that are related to showing a particular slide to their 
		default state.
	*/
	function clear() {
		bodyContent().innerHTML = ""; // clear the page (prepare for rendering the next slide)
		document.body.className = "";
	
		timeouts.forEach(function(timeout){clearTimeout(timeout);});
		timeouts = [];
	
		enteredSlideNumber = undefined;
		isBlackedOut = false;
		isOverviewShown = false;
		isListOfSlidesShown = false;
		isHelpShown = false;
	}


	function displaySlide(){
		clear();
		document.body.classList.add("ldjs-slide");
		
		// Activate keyboard presentation controls. (Recall that an event listener that is registered more than once is automatically discarded.)
		document.addEventListener("keyup", handlePresentationKeyEvent, true); 
				
		if (currentSlide < theSlides.length){
			console.log("Rendering slide: "+currentSlide);
			bodyContent().appendChild(theSlides[currentSlide]);
		} else {
			// TODO Improve the look of the "presentation finished." page.
			bodyContent().innerHTML="<div id='ldjs-black-layer'><div class='ldjs-presentation-finished'>Presentation finished.<br /> Press f to go to the first page.</div></div>";    
		}    	
		
		// We persist the id of the current slide to enable continuing the presentation
		// – after restart – where we left off...
		try{
			window.localStorage.setItem("lecturedoc-currentSlide-"+window.location.toString(),currentSlide);
		} catch (err) {
			console.log("Cannot store the information about the current slide: "+err+".");
		}
	}

		
	function handlePresentationKeyEvent(event) {
		// to make sure that a textarea element or input field can be used 
		// as expected (even if the event is "accidentally" propagated)
		var target = event.target.tagName;
		if( target === "TEXTAREA" || target === "INPUT" ) {
			return;
		}
			
		console.log("Key pressed: "+event.keyCode+".");

		// Special Actions:
		//
		switch (event.keyCode) {
			case 72/*h*:
			case 104/*H*/:
				if(isHelpShown){ 
					removeHelp();
				} else {
					addHelp();
				}
				return;
				
			case 73 /*i*/ :
			case 105 /*I*/ :
				if(isListOfSlidesShown){
					removeListOfSlides();
				}
				else {
					addListOfSlides();
				}
				return;
					
			case 66 /*b*/:
			case 98 /*B*/:
				// It is not possible to black out the last slide stating that
				// the presentation has finished.
				if(currentSlide < theSlides.length){
					if(isBlackedOut) {
						removeBlackLayer();	
					} else {
						addBlackLayer();
					}
				}
				return;
				
			case 67 /*c*/:
			case 99 /*C*/:			
				var canvas = document.getElementById("ldjs-canvas");
				if(window.getComputedStyle(canvas).getPropertyValue("visibility") === "hidden"){
					canvas.style.visibility = "visible";
				}
				else {
					canvas.style.visibility = "hidden";
				}
				return;
										
			case 79 /*o*/:
			case 108 /*O*/:
				if(isOverviewShown) {
					removeOverview();
				}
				else {
					addOverview();
				}
				return;	
		}
		
		/*
		 * Navigation of the slides
		 */
		switch (event.keyCode) {
			// go back
			case 33 /*page up*/ :
			case 37 /*left arrow*/ :
			case 38 /*up arrow*/ :
				if (currentSlide > 0) { 
					currentSlide -= 1;
				}
				break;				

			// jump to a specific slide or advance to the next slide
			case 13 /*Enter*/ : 			
				if (enteredSlideNumber) {
					if(enteredSlideNumber > theSlides.length) {
						currentSlide = theSlides.length - 1;
					} else {
						currentSlide = enteredSlideNumber - 1;
					}
					break;
				}
				/* deliberately fall through */
			// advance
			case 32 /*Space*/ :			
			case 34 /*page down*/ :
			case 39 /*right arrow*/ :
			case 40 /*down arrow*/ :
				if (currentSlide < theSlides.length) {
					currentSlide += 1;
				} 
				break;
				
			// go to the first slide
			case 70 /*f*/:			
			case 102 /*F*/:
				currentSlide = 0; 
				break;	

			// go to the last slide
			case 76 /*l*/:				
			case 108 /*L*/:
				currentSlide = theSlides.length - 1;
				break;
			// Handle the case that a number is entered or that the keystroke has
			// no further meaning.
			default:
				if(event.keyCode >= 48 && event.keyCode <= 57) {
					if (enteredSlideNumber) {
						enteredSlideNumber = enteredSlideNumber*10;
					}
					else {
						enteredSlideNumber = 0;
					}
					enteredSlideNumber += event.keyCode - 48;
				}
				return;
		}
		
		displaySlide();
   	}

	function addOverview() {
		var overview = document.createElement("ol");
		overview.id = "ldjs-slides-overview";
		bodyContent().appendChild(overview);			
		
		for (var slide =  0; slide < theSlides.length ; slide++) {
			var slideListItem = document.createElement("li");
			slideListItem.title = "Slide "+(slide+1);
			slideListItem.className = "ldjs-slides-overview-listitem";
			
			if (slide === currentSlide) {
				slideListItem.className = "ldjs-slides-overview-selected-slide";
			}
			
			addSlideOnClickListener(slideListItem, slide);
			
			// copy the slide to listitem and append it to overview
			slideListItem.appendChild(theSlides[slide].cloneNode(true));
			overview.appendChild(slideListItem);

			if(slide === currentSlide) {
				slideListItem.scrollIntoView();
			}
		}
	    
		isOverviewShown = true;
	}
	
	function removeOverview() {
		var element = document.getElementById("ldjs-slides-overview");
		bodyContent().removeChild(element);	
		isOverviewShown = false;		
	}
	
	/**
	 * Adds an onclick listener to jump to the provided slide number.
	 * Also sets the slider number as HTML5 data "data-ldjs-slide"
	 */
	function addSlideOnClickListener(element, slide) {
		element.setAttribute("data-ldjs-slide", slide);
		element.onclick = function(event) {
			var slide = parseInt(this.getAttribute("data-ldjs-slide"));
			jumpToSlide(slide)();
	    };
	}
	
	function addListOfSlides() {
		bodyContent().appendChild(theListOfSlides);
		isListOfSlidesShown = true;		
	}
	
	function removeListOfSlides() {
		bodyContent().removeChild(document.getElementById("ldjs-listofslides"));	
		isListOfSlidesShown = false;
	}	
		
	function addBlackLayer(){
		var blackLayer = document.createElement("div");
		blackLayer.id = "ldjs-black-layer";
		document.body.appendChild(blackLayer);
		isBlackedOut = true;
	}
	
	function removeBlackLayer(){
		document.body.removeChild(document.getElementById("ldjs-black-layer"));
		isBlackedOut = false;
	}
	
	function addMessageBox(id,innerHTML,onclose,timeout,className){
		var messageBox = document.createElement("div");
		messageBox.id = id;
		messageBox.innerHTML = innerHTML;
		if(onclose){ 
			messageBox.onclick = onclose; 
			if(timeout){ 
				timeouts.push(setTimeout(onclose,timeout)); 
			}			
		}
		else{
			if(timeout) {
				timeouts.push(
					setTimeout(function(){
						if(bodyContent().contains(messageBox)){
							bodyContent().removeChild(messageBox);
						}
					},
					timeout)
				);
			}
		}
		messageBox.className = className || "ldjs-message ldjs-message-information" ;
		bodyContent().appendChild(messageBox);
	}
	
	function addHelp(){
		addMessageBox(
			"ldjs-help",
			"<div style='border-bottom:1px solid #636363;padding-bottom: 0.5em;'><strong>Help</strong></div><br>m - change how the document is rendered<br><br>f - first slide<br>l - last slide<br>space, enter, right, down, page down  - next slide<br>left, up, page up - previous slide<br>b - black out<br>i - list of slides<br>o - overview<br>h - toggle this help<br>[0...9]<sup>*</sup> + enter - go to slide<br><br>ctrl + &lt;mouse movement&gt; - virtual laser pointer<br><br>c - (de-)activates canvas<br>alt + &lt;mouse movement&gt; - draws line if canvas is active<br><br>Last modified: <strong>"+(new Date(Number(document.body.dataset["ldjsLastModified"]))).toLocaleString()+"</strong><div style='font-size: smaller;margin-top:1em'>LectureDoc (c) 2013 Michael Eichberg et al.</div>",
			removeHelp,
			30000);
		isHelpShown = true;
	}
	
	function removeHelp() {
		var help = document.getElementById("ldjs-help");
		if (help){
			bodyContent().removeChild(help);	
			isHelpShown = false;
		}
	}
	
	function renderContinuousRepresentation(){
		clear();
		document.removeEventListener("keyup", handlePresentationKeyEvent, true); 
		
		document.body.classList.add("ldjs-continuous-layout");
		
		for(var s = 0 ; s < theSlides.length ; s++){
			bodyContent().appendChild(theSlides[s]);
		}
	}
	
	function renderLightTable(){
		clear();
		document.removeEventListener("keyup", handlePresentationKeyEvent, true); 
						
		document.body.classList.add("ldjs-light-table");
		
		for(var s = 0 ; s < theSlides.length ; s++){
			var lightTableSlide = document.createElement("div");
			addSlideOnClickListener(lightTableSlide, s);
			lightTableSlide.className = "ldjs-light-table-slide";
			lightTableSlide.appendChild(theSlides[s]);
			lightTableSlide.appendChild(document.createTextNode(String(s+1)));
			bodyContent().appendChild(lightTableSlide);
		}
	}
	
	function renderAsDocument() {
		clear(); 
		document.removeEventListener("keyup", handlePresentationKeyEvent, true); 
						
		document.body = theDocumentBody.cloneNode("true");
		
		var theSlides = toArray(document.querySelectorAll("section.slide")); 
		addSlideNumbers(theSlides);
	}
	
	function renderAsLectureNotes(){
		clear();
		document.removeEventListener("keyup", handlePresentationKeyEvent, true); 
						
		document.body.classList.add("ldjs-lecture-notes");
		
		// The effect of the following JavaScript method could also easily
		// be achieved using, e.g., XSLT.
		// grab the id="body-content" element from the body's clone, since it's the only child of the body element
		var body = theDocumentBody.cloneNode(true).querySelector('body>div#body-content');
			
		// 1. remove any whitespace between the body tag 
		//    and the first non-text element
		while (body.hasChildNodes()){
			if (body.firstChild.nodeType === Node.COMMENT_NODE ||
			 	(body.firstChild.nodeType === Node.TEXT_NODE && !/\S/.test(body.firstChild.data))){
			 	body.removeChild(body.firstChild);
			 	console.log("Removed whitespace from the beginning of the document.");
			 }
			 else {
			 	break;
			 }
		}
		
		// 2. create the lecture notes representation
		var lectureNotes = document.createElement("div");
		lectureNotes.id = "ldjs-lecture-notes";
		
		var subSection = document.createElement("div");
		subSection.className = "ldjs-lecture-notes-subsection";
		lectureNotes.appendChild(subSection);

		var sidebar;

		var content = document.createElement("div");
		content.className = "ldjs-lecture-notes-content";
		subSection.appendChild(content);
		
		while(body.hasChildNodes()){
			var currentNode = body.firstChild;
			body.removeChild(currentNode);
				
			if(currentNode.tagName === 'SECTION'){
				if(!/slide/.test(currentNode.className)){  // currentNode.className !== "slide"){
					var nextNode = body.firstChild;
					while(currentNode.hasChildNodes()){
						var node = currentNode.firstChild;
						currentNode.removeChild(node);
						body.insertBefore(node, nextNode);
					}	
									
					continue;
				}	

				// we create a new section
				subSection = document.createElement("div");
				subSection.className = "ldjs-lecture-notes-subsection";
				lectureNotes.appendChild(subSection);
				
				sidebar = document.createElement("div");
				sidebar.className = "ldjs-lecture-notes-sidebar";
				subSection.appendChild(sidebar);
				sidebar.appendChild(currentNode);

				var sectionBody = currentNode.querySelector("section.slide>div.section-body")
				if (sectionBody){
					var asides = sectionBody.querySelectorAll("div.section-body>aside");
					for (var i = 0 ; i < asides.length;i++){
						var aside = asides[i];
						sectionBody.removeChild(aside);
						sidebar.appendChild(aside);
					}
				}
				
				content = document.createElement("div");
				content.className = "ldjs-lecture-notes-content";
				subSection.appendChild(content);
			} 
			else {
				content.appendChild(currentNode);
			}
		}
	
		var footer = document.createElement("div");
		footer.id = "ldjs-lecture-notes-footer";
		footer.appendChild(document.createTextNode("Generated "+new Date()));

		bodyContent().appendChild(lectureNotes);
		bodyContent().appendChild(footer);
	}

	
	return { // The LectureDoc Object
		
		init : function(){
			analyzeDocument();
			
			if (theSlides.length == 0) {
				// LectureDoc is used as a simple markdown processer, it seems...
				renderAsDocument();
				return;
			}
						
			try{
				enableLectureDoc();
				var storageItem = window.localStorage.getItem("lecturedoc-currentSlide-"+window.location.toString())
				if (storageItem !== null) {
					var lastShownSlide = parseInt(storageItem);				
					console.log("Continuing presentation with last shown slide: "+lastShownSlide);
					setupPresentationMode(lastShownSlide);					
				} else {
					console.log("Starting the presentation with the first slide.");
					setupPresentationMode(0);											
					addHelp();
				}					
			} catch (err) { 
				console.log('Using "local storage" is not supported: '+err+".");		
				setupPresentationMode(0);
				addHelp();				
			}
		}
	};
}());





